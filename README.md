# Main backend technologies
* `PHP 7.3`
* `Laravel Framework 8.35.1`

# Main frontend technologies
* `React==17.0.2`
* `react-bootstrap==1.5.2`
* `bootstrap==4.6.0`

# Architecture
* `Backend is created as API service and frontend is a SPA created with create-react-app.`

# Notes
* `Coin Market API KEY variable should be defined in backend/.env as COIN_MARKET_API_KEY.`
* `For FE and BE communication an endpoint is defined in frontend/.env.local file as REACT_APP_API_ENDPOINT=http://localhost:8000 .`
* `Development environment was Laravel Homestead - Vagrant 2.2.15, VirtualBox 6.1.x`

# Main development phases
* `1) Setup Laravel 8 project, create a controller with a function that requests data from https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest , define a route where data is returned in JSON format.`
* `2) Setup a SPA with create-react-app, install bootstrap, axios and node-sass libraries, project logic setup, create components and style them.`
* `3) Create endpoint for FE communication with backend, create API GET requests with axios library and create timeout function for real-time data updates.`
