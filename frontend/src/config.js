const Config = {
  API_ENDPOINT: process.env.REACT_APP_API_ENDPOINT,
};

export default Config;
