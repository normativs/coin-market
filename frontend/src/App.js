import { Container } from "react-bootstrap";
import CoinTable from "components/coin-table/CoinTable";

function App() {
  return (
    <Container fluid>
      <Container>
        <CoinTable />
      </Container>
    </Container>
  );
}

export default App;
