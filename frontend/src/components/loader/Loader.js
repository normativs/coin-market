import { Row, Spinner, Col } from "react-bootstrap";
import "./styles.scss";

export default function Loader() {
  return (
    <>
      <Row className="loader fade-in ">
        <Spinner animation="grow" />
        <Spinner animation="grow" />
        <Spinner animation="grow" />
        <Spinner animation="grow" />
        <Spinner animation="grow" />
        <Spinner animation="grow" />
        <Spinner animation="grow" />
        <Spinner animation="grow" />
      </Row>
      <Row className="mt-3">
        <Col className="text-center text-uppercase bold-roboto tfs-small">
          Loading data...
        </Col>
      </Row>
    </>
  );
}
