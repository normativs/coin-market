import NumberFormat from "react-number-format";
import { FormatNr } from "./formatNr";

export default function Item({ data }) {
  return data.map((item, index) => (
    <tr key={index} className="item">
      <td className="text-center regular-roboto">
        <span className="mr-3 nr">{index + 1}</span>
        {item.symbol}
      </td>
      <td>
        <NumberFormat
          value={item.quote.USD.price.toFixed(2)}
          displayType={"text"}
          thousandSeparator={true}
          prefix={"$"}
        />
      </td>
      <td>
        <FormatNr num={item.quote.USD.market_cap} />
      </td>
      <td
        className={
          item.quote.USD.percent_change_24h >= 0
            ? "text-success"
            : "text-danger"
        }
      >
        {item.quote.USD.percent_change_24h.toFixed(2) + "%"}
      </td>
    </tr>
  ));
}
