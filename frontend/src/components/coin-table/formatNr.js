/**
 * Format big numbers function
 * @param {number} num
 * @returns {string}
 */

const FormatNr = ({ num }) => {
  if (num >= 1000000000 && num < 1000000000000) {
    num = (num / 1000000000).toFixed(2) + " billion";
  }
  if (num >= 1000000000000 && num < 1000000000000000) {
    num = (num / 1000000000000).toFixed(2) + " trillion";
  }
  return num;
};

export { FormatNr };
