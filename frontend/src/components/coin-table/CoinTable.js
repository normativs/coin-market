import { useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import { apiCoinMarket } from "api";
import Loader from "../loader/Loader";
import Item from "./Item";
import "./styles.scss";

export default function CoinTable() {
  const [loading, setLoading] = useState(true);
  const [coins, setCoins] = useState([]);

  // Fetching data every 30 seconds
  useEffect(() => {
    (function loop() {
      setLoading(true);
      apiCoinMarket().then((data) => {
        setCoins(data);
        setLoading(false);
      });
      setTimeout(loop, 30000);
    })();
  }, []);

  // Loader while fetching data
  if (loading) return <Loader />;

  // If no data is fetched
  if (Object.keys(coins).length === 0)
    return (
      <Table responsive className="mt-5">
        <thead className="text-uppercase regular-roboto tfs-small">
          <tr>
            <th className="text-center">No data</th>
          </tr>
        </thead>
      </Table>
    );

  return (
    <Table responsive className="mt-5 fade-in">
      <thead className="text-uppercase regular-roboto tfs-small">
        <tr>
          <th className="text-center">cryptocurrency</th>
          <th>price</th>
          <th>market cap</th>
          <th>24h</th>
        </tr>
      </thead>
      <tbody className="bold-roboto tfs-small">
        <Item data={coins.data} />
      </tbody>
    </Table>
  );
}
