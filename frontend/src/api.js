import axios from "axios";
import config from "config";

/**
 * API GET request
 * @param {string} url
 * @returns {object}
 */

const _GET = (url) => {
  return axios
    .get(config.API_ENDPOINT + url)
    .then(function (response) {
      return new Promise(function (resolve, reject) {
        resolve(response.data);
      });
    })
    .catch(function (error) {
      return new Promise(function (resolve, reject) {
        reject(error.response);
      });
    });
};

const apiCoinMarket = () => {
  return _GET("/api/index");
};

export { apiCoinMarket };
